\version "2.18.2"
\paper {
  
  system-system-spacing.basic-distance = #20
  system-system-spacing.minimum-distance = #20
  top-margin = 20	
  bottom-margin = 20
  left-margin = 8
  right-margin = 8
  %systems-per-page = 3
  
}

lvlv =  \markup \italic "l.v."


swOne = { \change Staff = "marimbaRightStaff" }
swTwo = { \change Staff = "marimbaLeftStaff" }

cTempo = #(define-music-function (parser location name noteValue metMark) (string? string? string?)
                 #{
                   #(customTempo name noteValue metMark)
                   
                 #})

spanish = #(define-music-function ( parser location name ) (string?)
             #{
               \override TextSpanner.bound-details.left.text = \markup \italic #name
             #} )

removeTextSpannerAfterBreak = 
{
 \once \override TextSpanner #'(bound-details left-broken text) = " " % works 
}

#(define (customTempo name noteValue metMark)
   (make-music
  'SequentialMusic
  'elements
  (list (make-music
          'TempoChangeEvent
          'metronome-count
          #f
          'tempo-unit
          #f
          'text
          (markup
            #:line
            (#:simple
             name
             #:hspace
             0.5
             #:override
             (cons (quote font-size) -2.5)
             (#:raise 0.35 (#:note noteValue 1))
             #:fontsize
             -1
             (#:normal-text "=")
             #:simple
             metMark))))))

xLV = #(define-music-function (parser location further) (number?) #{
     \once \override LaissezVibrerTie  #'X-extent = #'(0 . 0)
     \once \override LaissezVibrerTie  #'details #'note-head-gap = #(/
further -2)
     \once \override LaissezVibrerTie  #'extra-offset = #(cons (/
further 2) 0)
#}) 

#(define (crossyStaffy musicExpr)
    #{ 
    \voiceOne \autoBeamOff \crossStaff { #musicExpr } \oneVoice \autoBeamOn
    
    #} )

mbreak = {  }

\layout {
 % #(set-global-staff-size 12)
  %#(layout-set-staff-size 7) 
  ragged-right = ##f
  
  \context {
    \Staff
    \numericTimeSignature
    \override VerticalAxisGroup.staff-staff-spacing = #'((basic-distance . 10)(minimum-distance . .6)(padding . 2.5))    
    \override StaffSymbol.staff-space = #(magstep -2)
    fontSize = #-2
    %\RemoveEmptyStaves
    %\override StaffSymbol.thickness = #1
    \override TextSpanner.extra-offset = #'(0 . .5)
    
  }
  \context {
    \Voice
    \override BreathingSign.Y-offset = #2.5
  }
  
  \context {
   
    \StaffGroup
    \override StaffGrouper.staff-staff-spacing = #'((basic-distance . 6)(minimum-distance . 3)(padding . 2.5))
    %fontSize = #-2
    \override InstrumentName.font-size = #-2
  }
  
    \context {
   
    \GrandStaff
    \override StaffGrouper.staff-staff-spacing = #'((basic-distance . 6)(minimum-distance . 3)(padding . 2.5))
    %fontSize = #-2
    \override InstrumentName.font-size = #-2
    \consists #Span_stem_engraver
  }
  
      \context {
   
    \PianoStaff
    \override StaffGrouper.staff-staff-spacing = #'((basic-distance . 6)(minimum-distance . 3)(padding . 2.5))
    %fontSize = #-2
    \override InstrumentName.font-size = #-2
    \consists #Span_stem_engraver
  }
  
  \context {
   \Lyrics
   fontSize = #-.75
   \override LyricText.font-name = #"Garamond"
   
  }
  
  \context {
   \Score 
    \override TextSpanner.dash-fraction = #0.3
    \override TextSpanner.dash-period = #2
    \override MetronomeMark.font-size = #-1.5
    \override Hairpin.extra-offset = #'(0 . -.5)
    \override Hairpin.collision-padding = #8
    
    % old editions often have beams almost totally parallel to the notes.
    \override Beam #'damping = #0.5
    extraNatural = ##t
    % heavier appearance
    \override StaffSymbol #'thickness = #1.2
    \override Beam #'beam-thickness = #0.55
  }
  

}
