\version "2.19.81"
\include "paperlayout.ly"
\header {
  title = "NIGHTLETTER"
  subtitle = "III. Celebration and Dance"
   composer = \markup {   \center-column {  \line { \override #'(font-name . "Garamond Italic") "music by"  } \line  { \override #'(font-name . "Garamond") "Peter Engelbert" } } } 
  poet = \markup { \center-column { \line { \override #'(font-name . "Garamond Italic") "words by" } \line { \override #'(font-name . "Garamond") "Georgia Sanders" } } }
  tagline = ""
}

three_global = {
  \cTempo "Mockingly Sanctimonious" "2." "50"
  \time 3/4
  \key d \major
  \numericTimeSignature
  \grace s16
  s2. | %  2
  s2. | %  3
  s2. | %  4
  s2. | %  5
  s2. | %  6
  s2. | %  7
  s2. | %  8
  s2. | %  9
  s2. | %  10
  s2. | %  11
  s2. | %  12
  s2. | %  13
  s2. | %  14
  s2. | %  15
  s2. | %  16
  s2. | %  17
  s2. | %  18
  s2. | %  19
  s2. | %  20
  s2. | %  21
  s2. | %  22
  s2. | %  23
  s2. | %  24
  s2. | %  25
  s2. | %  26
  s2. | %  27
  s2. | %  28
  s2. | %  29
  s2. | %  30
  s2. | %  31
  s2. | %  32
  s2. | %  33
  s2. | %  34
  s2. | %  35
  s2. | %  36
  s2. | %  37
  s2. | %  38
  s2. | %  39
  \time 2/4
  s2 | %  40
  \time 3/4
  s2. | %  41
  s2. | %  42
  s2. | %  43
  s2. | %  44
  s2. | %  45
  s2. | %  46
  s2. | %  47
  s2. | %  48
  s2. | %  49
  s2. | %  50
  s2. | %  51
  s2. | %  52
  s2. | %  53
  s2. | %  54
  s2. | %  55
  s2. | %  56
  s2. | %  57
  s2. | %  58
  s2. | %  59
  s2. | %  60
  s2. | %  61
  s2. | %  62
  s2. | %  63
  s2. | %  64
  s2. | %  65
  s2. | %  66
  s2. | %  67
  s2. | %  68
  s2. | %  69
  s2. | %  70
  s2. | %  71
  s2. | %  72
  s2. | %  73
  s2. | %  74
  s2. | %  75
  s2. | %  76
  s2. | %  77
  s2. | %  78
  s2. | %  79
  s2. | %  80
  s2. | %  81
  s2. | %  82
  s2. | %  83
  s2. | %  84
  s2. | %  85
  s2. | %  86
  s2. | %  87
  s2. | %  88
  s2. | %  89
  s2. | %  90
  s2. | %  91
  s2. | %  92
  s2. | %  93
  s2. | %  94
  s2. | %  
}


three_mezzoI = \relative c'' {
  <<
    \three_global
    \dynamicUp
    % Music follows here.
    {
      \grace s16
      R2. * 10 | %  11
      r8 d\ff cis b a g | %  12
      fis r r4 r4 | %  13
      R2. | %  14
      fis2\ff e8 d | %  15
      a'2. | %  16
      a8 a r4 fis8 d | %  17
      d'4 cis8 b a4( | %  18
      b8 a) g( a) fis4 | %  19
      e2 ~ e8 r | %  20
      r b' cis( d) e( eis) | %  21
      fis2 e8 d | %  22
      a'2. ~ | %  23
      a2. | %  24
      r4 a,8 b cis4 ~ | %  25
      cis b8( cis) d4 | %  26
      a2.\sp\< | %  27
      a4->-.\sf r r | %  28
      R2.*4 | %  32
      r2 d4\mf | %  33
      cis2 cis4 | %  34
      c!2 c4 | %  35
      b2 bes4 | %  36
      a4 r r | %  37
      R2. | %  38
      r2 d4\mf ~ | %  39
      d8 a b g | %  40
      fis2 g4 | %  41
      a4 r8 e fis d | %  42
      e2 r4 | %  43
      g2 r4 | %  44
      es2 d4 | %  45
      r4 b'!8\f( a g fis) | %  46
      e8( d cis d) r4 | %  47
      e8( d cis d) r4 | %  48
      e8( d cis d) r4 | %  49
      gis8( fis e d cis b) | %  50
      R2. | %  51
      b'8( a gis fis e d) | %  51
    }
    >>
  }


three_mezzoILyrics = \lyricmode {
  % Lyrics follow here.
  Hip pip pip pip pip 
  pip!
  Hip cham -- pou -- ree!
  Hip -- hip cham -- pou -- ree!
  Polk it up  be -- hind me 
  Hip hip hip 
  Hip cham -- pou -- ree! 
  Polk it up __ be -- hind me!
  Hip!
  De -- fen -- dy nous from prowl -- a -- bouts
  Sing -- ty sang -- ty sing -- ty
  sing -- ty meek -- ly loose
  lu lu lu
  Jests! __ Jokes! __
  Jests! __  Jokes! __ 
  Jests! __ Jigs! __ 
}

three_mezzoII = \relative c'' {
  <<
    \three_global
    \dynamicUp
    % Music follows here.
    {
      \grace s16
      R2. * 11 | %  12
      r8\ff d cis b a g | %  13
      fis( g) e4( a) | %  14
      d,4 r cis8\ff b | %  15
      cis2. | %  16
      fis8 fis r4 d8 b | %  17
      b4 b8 b cis4 ~ | %  18
      cis b8([ cis)] d r | %  19
      a2.\p\< | %  20
      a'4->-.\sf r4 r | %  21
      d,8( e) fis( g) a( b) | %  22
      cis( b) a( g) fis( g) | %  23
      a a r4 fis8 d | %  24
      d'4 cis8 b a4( | %  25
      b8 a) g( a) fis( g | %  26
      e2 a4) | %  27
      d,4 r r | %  28
      R2.*2 | %  30
      d'4.\mf a8 b g | %  31
      fis2 g4 | %  32
      a4. e8 fis d | %  33
      e2 r4 | %  34
      g2 r4 | %  35
      es2 d4 | %  36
      r4 b'!8\f( a g fis) | %  37
      e( d cis4) d | %  38
      fis 4 e r | %  39
      R2 | %  40
      R2. | %  41
      r2 d'4\mf | %  42
      cis2 cis4 | %  43
      c!2 c4 | %  44
      b2 bes4 | %  45
      a4 r r | %  46
      r2 b4->\f | %  47
      r2 bes4-> | %  48
      r2 a4-> | %  49
      R2. | %  50
      a8( gis fis e d cis) | %  51
      R2. | %  52
      d'8( cis b a gis fis | %  53
      e fis e d) cis( b) | %  54
      a4 r r | %  55
      
    }
    >>
  }

 %%
three_mezzoIILyrics = \lyricmode {
  % Lyrics follow here.
  Hip pip pip pip pip 
  cham --  pou -- ree!
  Cham -- pou -- ree!
  Hip -- hip cham -- pou -- ree
  Polk it up be -- hind me!
  Hip!
  Hip  pip  pip 
  pip  pip  pip 
  Hip -- hip cham -- pou -- ree!
  Polk it up __ be -- hind __ me
  
  Sing -- ty sang -- ty sing -- ty
  sing -- ty meek -- ly loose
  lu lu lu 
  sing __ sing -- ty sang -- ty!
  De -- fen -- dy nous from prowl -- a -- bouts
  Jests! Jokes! Jigs! Jokes! __  Jor -- ums, Jigs!
}

three_guitarI = \relative c' {
  <<\three_global
    % Music follows here.
  { 
    \grace s16
    b'8\ff a g fis e d | %  2
  cis b a g fis e | %  3
  d4 d,2 | %  4
  r4 g\ff g | %  5
  a2.:32 | %  6
  << { \voiceTwo a2.\laissezVibrer } \\ { \voiceOne r8 e' fis g a b } >> | %  7
  \oneVoice
  cis8 d e fis g gis | %  8
  b a g! fis e d | %  9
  \pitchedTrill
  a'2.\startTrillSpan b  ~ | %  10
  a2. ~ | %  11
  a2. ~ | %  12
  a2. ~ | %  13
  a4 a\stopTrillSpan a | %  14
  fis8 a g fis e d | %  15
  e a, a2\startTrillSpan ~ | %  16
  a2. | %  17
  \grace s16\stopTrillSpan 
  R2.*11 | % 28  
  d'4.\f a8 b g | %  29
  fis2\>:32 g4:32 | %  30
  a2.\mp:32 | %  31
  }
  
  >>
}

three_guitarII = \relative c' {
  <<
    \three_global
    % Music follows here.
  {
    \grace s16
    b8\ff a g fis e d | %  2
    \pitchedTrill
    a'2.\startTrillSpan b | %  3
    d'2:32\stopTrillSpan cis8:16 b:16 | %  4
    a:16 d:16 cis:16 b:16 a:16 g:16 | %  5
    fis:16 g:16 e4-. e-. | %  6
    e2. | %  7
    R2. | %  8
    r8 fis\ff e d cis b | %  9
    cis b a g fis e | %  10
    d'2:32 cis8:16 b:16 | %  11
    a:16 d:16 cis:16 b:16 a:16 g:16 | %  12
    fis:16 fis:16 e:16 d:16 cis:16 b:16 | %  13
    a2.:32 | %  14
    d8 d' e fis g a | %  15
    cis b a g fis e | %  16
    fis a, b cis d e | %  17
    d8 r8 r4 r | %  18
    R2.*9 | %  27
    d4.\f a8 b g | %  28
    fis2:32 g4:32 | %  29
    a4\>:32 a':32 g:32  | %  30
    fis4\mp:32 r r | %  31
  }
  >>
}

three_guitarIII = \relative c' {
  <<\three_global
    % Music follows here.
  {
    \grace s16\ff <d,, a'>2. | %  2
    e'8 d cis b' a g | %  3
    fis a b cis d:16 e:16 | %  4
    fis:16 g:16 a:16 g:16 fis:16 e:16 | %  5
    d:16 e:16 cis4-. cis-. | %  6
    cis2. | %  7
    R2. | %  8
    R2. | %  9
    e8\ff d cis b' a g | %  10
    fis a, b cis d:16 e:16 | %  11
    fis:16 g:16 a:16 g:16 fis:16 e:16 | %  12
    d:16 d:16 cis:16 b:16 a:16 g:16 | %  13
    fis:16 g:16 e4-. cis | %  14
    d4 r r | %  15
    a8 b cis e d cis  | %  16
    d cis b g' fis e | %  17
    fis b, cis d e cis | %  18
    a e' d e a a' | %  19
    a a a a a a | %  20
    \once \override TrillSpanner.to-barline = ##t
    a2.\startTrillSpan | %  21
    \grace s16\stopTrillSpan
    R2.*8 | %  29
    r2 \grace s16\f <e, cis'>4 | %  30
    <d d'>2.\laissezVibrer
  }
  >>
}



three_mezzoIPart = \new Staff \with {
  instrumentName = "Sarah"
  midiInstrument = "flute"

} << \new voice = "SarahVoice" { \three_mezzoI } 
\addlyrics { \three_mezzoILyrics }
>>

three_mezzoIIPart = \new Staff \with {
  instrumentName = "Jasmine"
  midiInstrument = "flute"
} { \three_mezzoII }
\addlyrics { \three_mezzoIILyrics }


three_guitarIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Guitar I"
} { \clef "treble_8" \three_guitarI }

three_guitarIIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Guitar II"
} { \clef "treble_8" \three_guitarII }

three_guitarIIIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Guitar III"
} { \clef "treble_8" \three_guitarIII }


allSingers = \new StaffGroup <<
  \new Staff = "Sarah" \with {
    instrumentName = "Sarah"
    midiInstrument = "choir aahs"
  } << 
    \new Voice = "SV" { \three_mezzoI }
    \new Lyrics \lyricsto "SV" { \three_mezzoILyrics }
  >>
  \new Staff = "Jasmine" \with {
    instrumentName = "Jasmine"
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "JV" { \three_mezzoII }
  \new Lyrics \lyricsto "JV" { \three_mezzoIILyrics }
  >>
>>

allGuitars = \new StaffGroup <<
  \new Staff = "GuitarIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar I"
  } { \clef "treble_8" \three_guitarI }

  \new Staff ="GuitarIIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar II"
  } { \clef "treble_8" \three_guitarII }

  \new Staff = "GuitarIIIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar III"
  } { \clef "treble_8" \three_guitarIII }

>>

\score {
  <<
    \allSingers
    \allGuitars

  >>
  \layout { }
  \midi {
    \tempo 2.=50
  }
}
