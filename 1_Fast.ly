\version "2.18.2"
\include "paperlayout.ly"
\bookOutputName "/NIGHTLETTER/output/1_Fast"
\header {
  title = "NIGHTLETTER"
  subtitle = "I. Sonata"
  composer = \markup {   \center-column {  \line { \override #'(font-name . "Garamond Italic") "music by"  } \line  { \override #'(font-name . "Garamond") "Peter Engelbert" } } } 
  poet = \markup { \center-column { \line { \override #'(font-name . "Garamond Italic") "words by" } \line { \override #'(font-name . "Garamond") "NIGHTLETTER Collective" } } }
  tagline = ""
}
mbreak = {  }
\paper{
  #(set-paper-size "letter" 'portrait)
}

oneGlobal = {
  \cTempo "Frenetic" "4" "160"
  \numericTimeSignature
  \time 4/4
  \tag #'untransposed \key g \minor 
  s1 | %  1
  s1 | %  2
  s1 | %  3
  s1 \mark \markup { \right-align \bold "x4" } \bar ":|." | %  4
  s1 | %  5
  s1 | %  6
  s1 | %  7
  s1 | %  8
  s1 | %  9
  s1 | %  10
  s1 | %  11
  s1 \bar ".|:" | %  12
  s1  | % 13
  s1 | %  14
  s1 | %  15
  s1 \bar ":.|.:" | %  16
  s1  | %  17
  s1 | %  18
  s1 | %  19
  s1 \once \override Score.RehearsalMark.self-alignment-X = #RIGHT \mark \markup { \small \bold "Ad lib." } \bar ":|." | %  20
  s1 | %  21
  s1 | %  22
  s1 | %  23
  s1 | %  24
  s1 | %  25
  s1 | %  26
  s1 | %  27
  s1 | %  28
  s1 | %  29
  s1 | %  30
  s1 | \mbreak %  31
  s1 |  %  32
  s1 | %  33
  s1 | %  34
  s1 | %  35
  s1 | %  36
  s1 | %  37
  s1 | %  38
  s1 | \mbreak %  39
  s1  | %  40
  s1 | %  41
  s1 | %  42
  s1 | %  43
  s1 | %  44
  s1 | %  45
  s1 | %  46
  s1 | %  47
  s1 | \mbreak %  48
  s1 |  %  49
  s1 | %  50
  s1 | %  51
  s1 | %  52
  s1 | %  53
  s1 | %  54
  s1 | %  55
  s1 \mbreak | %  56
  s1  | %  57
  s1 | %  58
  s1 | %  59
  s1 | %  60
  s1 | %  61
  s1 | %  62
  s1 | %  63
  s1 \mbreak | %  64
  s1  | %  65
  s1 | %  66
  s1 | %  67
  s1 | %  68
  s1 | %  69
  s1 | %  70
  s1 | %  71
  s1 \mbreak | %  72
  s1 | %  73
  s1 | %  74
  s1 | %  75
  s1 | %  76
  s1 | %  77
  s1 | %  78
  s1 | %  79
  s1 | %  80
  s1 | %  81
  s1 \mbreak | %  82
  s1  | %  83
  s1 | %  84
  s1 | %  85
  s1 | %  86
  s1 | %  87
  s1 | %  88
  s1 \mbreak | %  89
  s1 | %  90
  s1 | %  91
  s1 | %  92
  s1 | %  93
  s1 | \mbreak %  94
  s1 \bar "||" | %  95
  \cTempo "A little more relaxed" "4" "128"   s1 | % 96
  s1 | % 97
  s1 | % 98
  s1 | % 99
  s1 | % 100
  s1 | % 101
  s1 \mbreak | % 102
  s1 | % 103
  s1 | % 104
  s1 | % 105
  s1 | % 106
  s1 | % 107
  s1 | % 108
  s1 | % 109
  s1 | % 110
  s1 | % 111
  s1 \mbreak | % 112
  s1 | % 113
  s1 | % 114
  s1 | % 115
  s1 | % 116
  s1 | % 117
  s1 | % 118
  s1 | % 119
  s1 | % 120
  s1 | \mbreak % 121
  s1 | % 122
  s1 | % 123
  s1 | % 124
  s1 | % 125
  s1 | % 126
  s1 | % 127
  s1 | % 128
  \time 3/4
  s2. 
  \tag #'untransposed \key f \minor 
  \bar "||" \mbreak | % 129
  \time 4/4
  s1 | % 130
  s1 | % 131
  s1 | % 132
  s1 | % 133
  s1 | % 134
  s1 | % 135
  s1 | % 136
  s1 | % 137
  s1 \mbreak | % 138
  s1 | % 139
  s1 | % 140
  s1 | % 141
  s1 | % 142
  s1 | % 143
  s1 | % 144
  s1 | % 145
  s1 \mbreak | % 146
  s1 | % 147
  s1 | % 148
  s1 | % 149
  s1 | % 150
  s1 | % 151
  s1 | % 152
  s1 \mbreak | % 153
  s1 | % 154
  s1 | % 155
  s1 \bar "||"| % 156
  \tag #'untransposed \key dis \minor
  s1 | % 157
  s1 | % 158
  s1 | % 159
  s1 \mbreak | % 160
  s1 | % 161
  s1 | % 162
  s1 | % 163
  s1 | % 164
  s1 | % 165
  s1 | % 166
  \bar "||"
  \tag #'untransposed \key g \major
  s1 \mbreak | % 167
  s1 | % 168
  s1 | % 169
  s1 | % 170
  s1 | % 171
  s1 | % 172
  s1 | % 173
  s1 | % 174
  s1 \mbreak | % 175
  s1 | % 176
  s1 | % 177
  s1 | % 178
  s1 | % 179
  s1 | % 180
  s1 | % 181
  s1 | % 182
  s1 \mbreak | % 183
  s1 | % 184
  s1 | % 185
  \bar "||"
  \tag #'untransposed \key g \minor
  s1 | % 186
  s1 | % 187
  s1 | % 188
  s1 | % 189
  s1 \mbreak | % 190
  s1 | % 191
  s1 | % 192
  s1 | % 193
  s1 | % 194
  s1 | % 195
  s1 \mbreak | % 196
  s1 | % 197
  s1 | % 198
  s1 | % 199
  s1 | % 200
  s1 | % 201
  s1 | \mbreak % 202
  s1 | % 203
  s1 | % 204
  s1 | % 205
  s1 | % 206
  s1 | % 207
  s1 | % 208
  s1 | % 209
  s1 | % 210
  s1 | % 211
  s1 | % 212
  s1 | % 213
  s1 | % 214
  s1 | % 215
  s1 | % 216
  s1 | % 217
  s1 | % 218
  s1 | % 219
  s1 | % 220
  s1 | % 221
  s1 | % 222
  s1 | % 223
  s1 | % 224
  s1 | % 225
  s1 \bar "|." % 226
}


oneMezzoI = \relative c'' {
  <<
    \oneGlobal
    \dynamicUp
    % Music follows here.
    {
      R1 | % 1
      R1 | % 2
      R1 | % 3
      R1 | % 4
      R1 | % 5
      R1 | % 6
      R1 | % 7
      R1 | % 8
      R1 | % 9
      R1 | % 10
      R1 | % 11
      R1 | % 12
      R1 | % 13
      R1 | % 14
      R1 | % 15
      R1 | % 16
      R1 | % 17
      R1 | % 18
      R1 | % 19
      R1 | % 20
      R1 | % 21
      R1 | % 22
      R1 | % 23
      R1 | % 24
      R1 | % 25
      R1 | % 26
      R1 | % 27
      R1 | % 28
      R1 | % 29
      R1 | % 30
      R1 | % 31
      R1 | % 32
      R1 | % 33
      d2 c | % 34
      bes a | % 35
      bes a8 bes a g~ | % 36
      g2 r2 | % 37
      d'2 c | % 38
      bes a | % 39
      bes a8 bes a g~ | % 40
      g2 r2 | % 41
      R1 | % 42
      R1 | % 43
      R1 | % 44
      R1 | % 45
      R1 | % 46
      R1 | % 47
      R1 | % 48
      R1 | % 49
      R1 | % 50
      R1 | % 51
      R1 | % 52
      R1 | % 53
      R1 | % 54
      R1 | % 55
      R1 | % 56
      R1 | % 57
      R1 | % 58
      R1 | % 59
      bes2 a8( bes a) g~  | % 60
      g2 r2 | % 61
      d'2 c | % 62
      bes a | % 63
      bes a8 bes a g~ | % 64
      g2 r2 | % 65
      d'2 c  | % 66
      bes a  | % 67
      bes a8( bes a) g~ | % 68
      g1 | % 69
      R1 | % 70
      R1 | % 71
      R1 | % 72
      R1 | % 73
      R1 | % 74
      R1 | % 75
      R1 | % 76
      g'2 d | % 77
      f c | % 78
      g' d | % 79
      f( c) | % 80
      R1 | % 81
      R1 | % 82
      R1 | % 83
      R1 | % 84
      g'2 d | % 85
      f c | % 86
      g' d | % 87
      f( c) | % 88
      g'4 d f( c) | % 89
      g' d f( c) | % 90
      R1 | % 91
      R1 | % 92
      R1 | % 93
      R1 | % 94
      R1 | % 95
      R1 | % 96
      r4 bes f'2 | % 97
      es8 d c bes r4 d | % 98
      a4. g8 a4 bes | % 99
      \tuplet 3/2 { es4 d c } bes4 a8 r8 | % 100
      bes2 a | % 101
      g f4. r8 | % 102
      es2 d | % 103
      c fis | % 104
      g4. r8 f2 | % 105
      bes, es | % 106 
      f2. r4 | % 107
      bes2 a  | % 108
      g f4. r8 | % 109
      es2 d | % 110
      c f | % 111
      r4 bes f'2 | % 112
      es8 d c bes r4 d | % 113
      a4. g8 a4 bes | % 114
      \tuplet 3/2 { es4 d c~ c bes a~ } | % 115
      a4 bes r c | % 116
      es d~ d8 c d es | % 117
      bes2 a  | % 118
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      bes1\laissezVibrer | % 119
      R1 | % 120
      R1 | % 121
      R1 | % 122
      R1 | % 123
      R1 | % 124
      R1 | % 125
      R1 | % 126
      R1 | % 127
      R1 | % 128
      R2. | % 129
      R1 | % 130
      R1 | % 131
      R1 | % 132
      R1 | % 133
      as4. bes8~ bes2 | % 134
      c4 bes8 c~ c2 | % 135
      f2 es8 f4 c8~ | % 136
      c4. r8 des4 c8 bes~ | % 137
      bes as4. ~ as4 bes | % 138
      c2. r4 | % 139
      R1 | % 140
      R1 | % 141
      as4. bes8~ bes2 | % 142
      c4 r4 r2 | % 143
      R1 | % 144
      es2 des8 es4 bes8~ | % 145
      bes2 r2 | % 146
      e'2 d8 e4.~ | % 147
      e4 f r2 | % 148
      as,1~ | % 149
      as\breathe | % 150
      as | % 151
      g\breathe | % 152
      f1~ | % 153
      f1 | % 154
      R1 | % 155
      R1 | % 156
      R1 | % 157
      R1 | % 158
      R1 | % 159
      r2 gis,4. ais8 | % 160
      b4 ais r2 | % 161
      r2 b4. fis'!8~ | % 162
      fis2. e4 | % 163
      cis!2 b4 d | % 164
      c!2 r4 b | % 165
      a2 r2 | % 166
      r4 g! d'!2 | % 167
      c8 b a g r4 b | % 168
      fis4. e8 fis4 g | % 169
      \tuplet 3/2 { c4 b a } g4 fis | % 170
      r4 g fis2 | % 171
      e d4. r8 | % 172
      c2 b | % 173
      a dis | % 174
      e fis4. r8 | % 175
      g2. a4~ | % 176
      a1 | % 177
      r2 r4 d | % 178
      g2 fis8 e d c | % 179
      r4 e b4. a8 | % 180
      b4 c a2 | % 181
      r4 g d'2 | % 182
      c8 b a g r4 b | % 183
      fis4. e8 fis4 g | % 184
      \tuplet 3/2 { c4 b a } g4 fis | % 185
      g1~ | % 186
      g4 r4 r2 | % 187
      R1 | % 188
      R1 | % 189
      R1 | % 190
      R1 | % 191
      R1 | % 192
      R1 | % 193
      R1 | % 194
      R1 | % 195
      R1 | % 196
      R1 | % 197
      R1 | % 198
      R1 | % 199
      R1 | % 200
      R1 | % 201
      R1 | % 202
      R1 | % 203
      R1 | % 204
      R1 | % 205
      R1 | % 206
      R1 | % 207
      R1 | % 208
      R1 | % 209
      R1 | % 210
      R1 | % 211
      R1 | % 212
      R1 | % 213
      R1 | % 214
      R1 | % 215
      R1 | % 216
      R1 | % 217
      R1 | % 218
      R1 | % 219
      R1 | % 220
      R1 | % 221
      R1 | % 222
      R1 | % 223
      R1 | % 224
      R1 | % 225
      R1 | % 226
    }
  >>
}


oneMezzoILyrics = \lyricmode {
  % Lyrics follow here.

}

oneMezzoII = \relative c'' {
  <<
    \oneGlobal
    \dynamicUp
    % Music follows here.
    {
      R1 | % 1
      R1 | % 2
      R1 | % 3
      R1 | % 4
      R1 | % 5
      R1 | % 6
      R1 | % 7
      R1 | % 8
      R1 | % 9
      R1 | % 10
      R1 | % 11
      R1 | % 12
      R1 | % 13
      R1 | % 14
      R1 | % 15
      R1 | % 16
      R1 | % 17
      R1 | % 18
      R1 | % 19
      r2 r4 r8 bes, | % 20
      bes4. c8~ c2 | % 21
      d4 c8 d8~ d2 | % 22
      g2 f8 g4 d8~ | % 23
      d2 es4 d8 c8~ | % 24
      c8 bes4.~ bes4 c | % 25
      d2~\< d8-.->\! r8 r4 | % 26
      R1 | % 27
      R1 | % 28
      bes4. c8~ c2 | % 29
      d4 c8 d~ d2 | % 30
      g f8 g4 d8~ | % 31
      d2 es4 d8 c~ | % 32
      c bes4.~ bes4 c | % 33
      d2~\< d8-.->\! r8 r4 | % 34
      R1 | % 35
      R1 | % 36
      R1 | % 37
      R1 | % 38
      R1 | % 39
      R1 | % 40
      bes4. c8~ c2 | % 41
      d4 c8 d~ d2 | % 42
      g2 f8 g4 d8~ | % 43
      d2 es4 d8 c~ | % 44
      c bes4. ~ bes4 c | % 45
      d2\> r2\! | % 46
      R1 | % 47
      R1 | % 48
      bes4 bes8 c~ c2 | % 49
      d4 c8 d~ d2 | % 50
      g f8 g4 d8~ | % 51
      d2 r2 | % 52
      bes8 bes bes bes a a a bes~ | % 53
      bes bes bes bes a a a bes | % 54
      r bes bes bes c c c c  | % 55
      d2 c8 d c bes~ | % 56
      bes bes bes bes a a a  bes~ | % 57
      bes bes bes r a a a bes~ | % 58
      bes bes bes bes c c c r | % 59
      d2 c8 d c bes~ | % 60
      bes bes bes bes a a a bes | % 61
      r bes bes bes a a a bes | % 62
      r bes bes bes c c c c | % 63
      d2 c8 d c bes~ | % 64
      bes bes bes bes a a a bes~ | % 65
      bes bes bes r a a a bes~ | % 66
      bes bes bes bes c c c r | % 67
      d2 c8( d c) bes~ | % 68
      bes1 | % 69
      R1 | % 70
      R1 | % 71
      R1 | % 72
      R1 | % 73
      R1 | % 74
      R1 | % 75
      R1 | % 76
      bes'2 g | % 77
      es a | % 78
      bes g | % 79
      es( a) | % 80
      R1 | % 81
      R1 | % 82
      R1 | % 83
      R1 | % 84
      g2 bes | % 85
      d c | % 86
      g bes | % 87
      d( c) | % 88
      g4 bes d( c) | % 89
      g bes d( c) | % 90
      R1 | % 91
      R1 | % 92
      R1 | % 93
      R1 | % 94
      R1 | % 95
      R1 | % 96
      bes2 a | % 97
      g f4. r8 | % 98
      es2 d | % 99
      c f | % 100
      r4 bes f'2 | % 101
      es8 d c bes r4 d | % 102
      a4. g8 a4 bes | % 103
      \tuplet 3/2 { es4 d c } bes4 a~ | % 104
      a bes r c  | % 105
      es d~ d8 c d es | % 106
      bes2 a | % 107
      r4 bes f'2 | % 108
      es8 d c bes r4 d | % 109
      a4. g8 a4 bes | % 110
      \tuplet 3/2 { es4 d c } bes4 a8 r8  | % 111
      bes2 a | % 112
      g2 f4. r8 | % 113
      es2 d c fis | % 114
      g4. r8 f2 | % 115
      bes es | % 116
      f es | % 117
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      d1^\laissezVibrer | % 118
      R1 | % 119
      R1 | % 120
      R1 | % 121
      R1 | % 122
      R1 | % 123
      R1 | % 124
      R1 | % 125
      R1 | % 126
      R1 | % 127
      R2. | % 129
      R1 | % 130
      R1 | % 131
      R1 | % 132
      R1 | % 133
      R1 | % 134
      R1 | % 135
      R1 | % 136
      R1 | % 137
      R1 | % 138
      c2 bes | % 139
      as g | % 140
      as g8 as g f~ | % 141
      f2 r2 | % 142
      c'4 bes8 c~ c2 | % 143
      f2 es8 f4 c8~ | % 144
      c2 r2  | % 145
      g'2 f8 g4 c,8~ | % 146
      c1 | % 147
      R1 | % 148
      R1 | % 149
      R1 | % 150
      R1 | % 151
      R1 | % 152
      R1 | % 153
      R1 | % 154
      R1 | % 155
      R1 | % 156
      r4 dis, ais'2 | % 157
      gis8 fis eis dis r4 fis | % 158
      cis4. b8 cis4 dis | % 159
      gis8 fis eis dis gis4 r | % 160
      r4 fis cis'2 | % 161
      b8 ais gis fis b4. a8 | % 162
      g2 r4 b~ | % 163
      b ais! b2~ | % 164
      b4 e, a g~ | % 165
      g fis8 e fis2 | % 166
      r4 g fis2 | % 167
      e2 d4. r8 | % 168
      c2 b2 | % 169
      a d | % 170
      r4 g d'2 | % 171
      c8 b a g r4 b | % 172
      fis4. e8 fis4 g | % 173
      \tuplet 3/2 { c4 b a } g4 fis~ | % 174
      fis g r a | % 175
      c4 b~ b8 a b c | % 176
      g2 fis | % 177
      r4 g d'2 | % 178
      c8 b a g r4 b | % 179
      fis4. e8 fis4 g | % 180
      \tuplet 3/2 { c4 b a } g4 fis | % 181
      r2 r4 d' | % 182
      g2 fis8 e d c | % 183
      r4 e4 b4. a8 | % 184
      b4 c a2 | % 185
      bes!1~ | % 186
      bes4 r4 r2 | % 187
      R1 | % 188
      R1 | % 189
      R1 | % 190
      R1 | % 191
      R1 | % 192
      R1 | % 193
      R1 | % 194
      R1 | % 195
      R1 | % 196
      R1 | % 197
      R1 | % 198
      R1 | % 199
      R1 | % 200
      R1 | % 201
      R1 | % 202
      R1 | % 203
      R1 | % 204
      R1 | % 205
      R1 | % 206
      R1 | % 207
      R1 | % 208
      R1 | % 209
      R1 | % 210
      R1 | % 211
      R1 | % 212
      R1 | % 213
      R1 | % 214
      R1 | % 215
      R1 | % 216
      R1 | % 217
      R1 | % 218
      R1 | % 219
      R1 | % 220
      R1 | % 221
      R1 | % 222
      R1 | % 223
      R1 | % 224
      R1 | % 225
      R1 | % 226
    }
  >>
}

%%
oneMezzoIILyrics = \lyricmode {
  % Lyrics follow here.
  
}

oneGuitarI = \relative c' {
  <<\oneGlobal
    % Music follows here.
    
    {
      R1 | % 1
      R1 | % 2
      R1 | % 3
      R1 | % 4
      R1 | % 5
      R1 | % 6
      R1 | % 7
      R1 | % 8
      R1 | % 9
      R1 | % 10
      R1 | % 11
      bes'2 a8 bes a g~ | % 12
      g1 | % 13
      d'2 c | % 14
      bes a | % 15
      bes a8 bes a g~ | % 16
      g1 | % 17
      R1 | % 18
      R1 | % 19
      R1 | % 20
      R1 | % 21
      R1 | % 22
      R1 | % 23
      R1 | % 24
      R1 | % 25
      R1 | % 26
      R1 | % 27
      R1 | % 28
      R1 | % 29
      R1 | % 30
      R1 | % 31
      R1 | % 32
      R1 | % 33
      R1 | % 34
      R1 | % 35
      R1 | % 36
      bes8 bes bes a~ a a a a | % 37
      bes bes bes a~ a a a a | % 38
      g g g f~ f f f f | % 39
      g g g g a g a bes | % 40
      bes bes bes a~ a a a a | % 41
      bes bes bes a~ a a a a | % 42
      g g g f~ f f f f  | % 43
      g g g g a g a bes | % 44
      bes bes bes a~ a a a a | % 45
      bes bes bes a~ a a a a | % 46
      g g g f~ f f f f | % 47
      g g g g a g a bes | % 48
      bes bes bes a~ a a a a | % 49
      bes bes bes a~ a a a a | % 50
      g g g f~ f f f f  | % 51
      g g g g a g f es  | % 52
      d d d d c c c d~ | % 53
      d d d d c c c d~ | % 54
      d d d d c c c c  | % 55
      d d d d es es es es | % 56
      d8 d d d c c c d~ | % 57
      d d d d c c c d~ | % 58
      d d d d c c c c  | % 59
      d d d d es es es es | % 60
      d d d d c c c d~ | % 61
      d d d d c c c d~ | % 62
      d d d d c c c c  | % 63
      d d d d es es es es | % 64
      d d d d c c c d~ | % 65
      d d d d c c c d~ | % 66
      d d d d c c c c | % 67
      d d d d es es es a | % 68
      bes bes bes bes g g g g | % 69
      es es es es a a a a  | % 70
      bes bes bes bes g g g g | % 71
      es es es d es es es a | % 72
      bes bes bes bes g g g g | % 73
      es es es es a a a a | % 74
      bes bes bes bes g g g g | % 75
      es es es es a a a a | % 76
      R1 | % 77
      R1 | % 78
      R1 | % 79
      R1 | % 80
      g8 g g g bes bes bes bes  | % 81
      d d d d c c c c | % 82
      g g g g bes bes bes bes | % 83
      d d d d c c c c | % 84
      \repeat unfold 8 g8 | % 85
      g8 g g g f f f f | % 86
      \repeat unfold 8 g8 | % 87
      g8 g g g f f f f | % 88
      \ottava #1
      g' bes, d g, f' es, c' a | % 89
      g' bes, d g, f' es, c' a | % 90
      \repeat unfold 3 { g'16 bes, d g, f' es, c' a  } % | 91
      es' g, bes es, d' d, a' f | % 92
      \repeat unfold 2 { c' es, g c, bes' bes, d f } | % 93
      \override TextSpanner.bound-details.left.text = \markup \italic "rit."
      c'\startTextSpan es, ges bes, c' es, ges bes, \repeat unfold 2 { \relative c'' { c es, ges bes, } } | % 94
      c'8 es, ges bes, c' es, ges bes,~ | % 95
      bes2\stopTextSpan \ottava #0 r2 | % 96
      R1 | % 97
      R1 | % 98
      R1 | % 99
      r2 r8 f' g a | % 100
      bes d c bes c es, d c | % 101
      bes bes' a g a f es d | % 102
      c8 bes a g f d' es f | % 103
      g es a bes c2~ | % 104
      c8 d c bes a g f es | % 105
      f f, g a bes g bes es | % 106
      d c bes d c d es f | % 107
      d d' c bes c es, d c | % 108
      bes bes' a g a f es d  | % 109
      c bes a g f d' es f | % 110
      g es a bes c2 | % 111
      bes8 d c bes c es, d c | % 112
      bes8 bes' a g a f es d | % 113
      c bes a g f d' es f  | % 114
      g es a bes c2~ | % 115
      c8 d c bes a g f es f f, g a bes g bes es | % 116
      d c bes d c d es f | % 117
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      d1^\markup \italic "l.v."\laissezVibrer | % 118
      R1 | % 119
      R1 | % 120
      R1 | % 121
      R1 | % 122
      es8^\markup \italic "medium dist." des, as' g' es des, as' g' | % 124
      R1 | % 125
      a8 des, g des a' des, g des | % 126
      r2 g8 bes fis g | % 127
      r4 g8 bes r4 fis8 g | % 128
      g8 bes r4 fis8 g | % 129
      R1 | % 130
      R1 | % 131
      R1 | % 132
      R1 | % 133
      R1 | % 134
      R1 | % 135
      R1 | % 136
      R1 | % 137
      R1 | % 138
      R1 | % 139
      R1 | % 140
      R1 | % 141
      \repeat percent 3 { \relative c' { r8 c f as~ as g as ces } } | % 142-145
      r8 c, es f~ f es f a  | % 145
      r8 des, f bes~ bes f des f | % 146
      e' des c b bes as g f | % 147
      e des c bes as g f es! | % 148
      f es des c des2 | % 149
      R1 | % 150
      R1 | % 151
      R1 | % 152
      R1 | % 153
      R1 | % 154
      R1 | % 155
      R1 | % 156
      R1 | % 157
      R1 | % 158
      R1 | % 159
      R1 | % 160
      R1 | % 161
      R1 | % 162
      R1 | % 163
      R1 | % 164
      R1 | % 165
      r8 g a b c d e fis! | % 166
      g b a g a c, b a | % 167
      g8 g' fis e fis d c b | % 168
      a g fis e d b' c d | % 169
      e c fis g a2 | % 170
      R1 | % 171
      R1 | % 172
      R1 | % 173
      R1 | % 174
      R1 | % 175
      R1 | % 176
      R1 | % 177
      g,8 d g b d e fis d | % 178
      c b a c b b, c d  | % 179
      e c d e fis d g d | % 180
      a' g fis e d4 d' | % 181
      g8 d g b \ottava #1 d e fis d  | % 182
      c b a c b \ottava #0 b, c d | % 183
      e8 c d e fis d g d | % 184
      a' g fis e d4 d,~ | % 185
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      d1^\lvlv\laissezVibrer | % 186
      R1 | % 187
      R1 | % 188
      R1 | % 189
      R1 | % 190
      R1 | % 191
      R1 | % 192
      R1 | % 193
      R1 | % 194
      R1 | % 195
      R1 | % 196
      R1 | % 197
      R1 | % 198
      R1 | % 199
      R1 | % 200
      R1 | % 201
      R1 | % 202
      R1 | % 203
      R1 | % 204
      R1 | % 205
      R1 | % 206
      R1 | % 207
      R1 | % 208
      R1 | % 209
      R1 | % 210
      R1 | % 211
      R1 | % 212
      R1 | % 213
      R1 | % 214
      R1 | % 215
      R1 | % 216
      R1 | % 217
      R1 | % 218
      R1 | % 219
      R1 | % 220
      R1 | % 221
      R1 | % 222
      R1 | % 223
      R1 | % 224
      R1 | % 225
      R1 | % 226
    }
  >>
}

oneGuitarII = \relative c' {
  <<
    \oneGlobal
    % Music follows here.
    {
      R1 | % 1
      R1 | % 2
      R1 | % 3
      R1 | % 4
      r2 a2 | % 5
      bes a | % 6
      bes a8 a a a | % 7
      d2 c8 d c bes~ | % 8
      bes bes bes bes a a a bes~ | % 9
      bes bes bes bes a a a bes~ | % 10
      bes bes bes bes a a a a | % 11
      d2 c8 d c bes~ | % 12
      bes bes bes bes a a a bes~ | % 13
      bes bes bes bes a a a bes~ | % 14
      bes bes bes bes c c c c | % 15
      d2 c8 d c  bes~  | % 16
      bes1 | % 17
      R1 | % 18
      R1 | % 19
      R1 | % 20
      R1 | % 21
      R1 | % 22
      R1 | % 23
      R1 | % 24
      R1 | % 25
      R1 | % 26
      R1 | % 27
      R1 | % 28
      R1 | % 29
      R1 | % 30
      R1 | % 31
      R1 | % 32
      r2 r4 r8 bes~ | % 33
      bes bes bes bes a a a bes~ | % 34
      bes bes bes bes c c c c | % 35
      d d d d c8 d c bes | % 36
      \repeat percent 2 { <d, bes'>4. <a' c>8~ q2 } | % 37 & 38
      <d, bes'>4. <c a'>8~ q2 | % 39
      <d f> <c es> | % 40
      \repeat percent 3 { <bes' d>4. <a f'>8~ q2 } | % 41-43
      <bes g'>2 <c es> | % 44
      \repeat percent 2 { <bes g'>4. <c f>8~ q2 } | % 45-46
      <bes g'>4. <c a'>8~ q2 | % 47
      <d bes'> <c a'> | % 48
      \repeat percent 2 { <bes g'>4. <c f>8~ q2 } | % 49
      <bes g'> 4. <c a'>8~ q2 | % 51
      <d bes'>2 <c a'> | % 52
      <bes g'>1 | % 53
      R1 | % 54
      R1 | % 55
      g8 g g g a a a a | % 56
      bes bes bes bes a a a bes~ | % 57
      bes bes bes bes a a a bes~ | % 58
      bes bes bes bes a a a a  | % 59
      g g g g a a a a | % 60
      bes bes bes bes a a a bes~ | % 61
      bes bes bes bes a a a bes~ | % 62
      bes bes bes bes a a a a  | % 63
      g g g g a a a a | % 64
      bes bes bes bes a a a bes~ | % 65
      bes bes bes bes a a a bes~ | % 66
      bes bes bes bes a a a a | % 67
      g g g g a a a a | % 68
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      g1^\markup \italic "l.v., full decay"\laissezVibrer | % 69
      R1 | % 70
      R1 | % 71
      R1 | % 72
      g'8 g g g bes bes bes bes | % 73
      d d d d c c c c | % 74
      g g g g bes bes bes bes | % 75
      d d d d c c c c | % 76
      R1 | % 77
      R1 | % 78
      R1 | % 79
      R1 | % 80
      g8 g g g d d d d | % 81
      f f f f c c c c | % 82
      g'8 g g g d d d d | % 83
      f f f f c c c c | % 84
      d d d d bes bes bes bes | % 85
      g g g g f' f f f | % 86
      d d d d bes bes bes bes | % 87
      g g g g f' f f f | % 88
      g, d' a bes a g a c | % 89
      g d' a bes a g a c | % 90
      R1 | % 91
      R1 | % 92
      R1 | % 93
      R1 | % 94
      R1 | % 95
      r8 bes c d es f g as| % 96
      bes d c bes c es, d c | % 97
      bes bes' a g a f es d | % 98
      c bes a g f d' es f  | % 99
      g es a bes c2 | % 100
      R1 | % 101
      R1 | % 102
      R1 | % 103
      R1 | % 104
      R1 | % 105
      R1 | % 106
      R1 | % 107
      bes,8 f bes d f g a f | % 108
      es d c es d d, es f  | % 109
      g es f g a f bes f | % 110
      c' bes a g f4 f' | % 111
      bes8 f bes d \ottava #1 f g a f | % 112
      es d c es d d, es f  | % 113
      g es f g a f bes f | % 114
      c bes a g a2~ | % 115
      a8 \ottava #0 bes' a g f a c4~ | % 116
      c bes8 a g bes a g f f, f f f bes c c | % 117
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      bes1^\markup \italic "l.v."\laissezVibrer | % 118
      R1 | % 119
      R1 | % 120
      R1 | % 122
      R1 | % 123
      e8^\markup \italic "medium dist." c, g' as' e c, g' as' | % 124
      R1 | % 125
      e,8 c' bes c c, c' bes c | % 126
      r2 fis8 e des e | % 127
      r4 fis8 e r4 des8 e | % 128
      fis8 e r4 des8 e | % 129
      R1 | % 130
      R1 | % 131
      R1 | % 132
      R1 | % 133
      R1 | % 134
      R1 | % 135
      R1 | % 136
      R1 | % 137
      r8 as, bes ces~ ces bes ces es | % 138
      r8 as, bes ces~ ces bes ces es | % 139
      r8 as, bes ces~ ces bes ces es~ | % 140
      es bes ces es~ es d des ces | % 141
      r8 as bes ces~ ces bes ces es | % 142
      r8 as, bes ces~ ces bes ces es | % 143
      r8 as, bes ces~ ces bes ces es | % 144
      r8 es f a~a g a c | % 145
      r8 f,, bes des~ des b g b | % 146
      r4 c,2.~\trill | % 147
      c1~ | % 148
      c4 r4 <des as' des>2~\f | % 149
      q1 | % 150
      <es bes' es>~ | % 151
      q1 | % 152
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      <f c' f>^\markup \italic "l.v."\laissezVibrer | % 153
      R1 | % 154
      R1 | % 155
      R1 | % 156
      R1 | % 157
      R1 | % 158
      R1 | % 159
      R1 | % 160
      R1 | % 161
      R1 | % 162
      R1 | % 163
      R1 | % 164
      R1 | % 165
      R1 | % 166
      R1 | % 167
      R1 | % 168
      R1 | % 169
      r2 r8 d' e fis | % 170
      g b a g a c, b a  | % 171
      g g' fis e fis d c b | % 172
      a g fis e d b' c d  | % 173
      e c fis g a2~ | % 174
      a8 b a g fis e d c | % 175
      d8 d, e fis g e g c | % 176
      b a g b a b c d | % 177
      b b' a g a c, b a | % 178
      g g' fis e fis d c b | % 179
      a g fis e d b' c d | % 180
      e c fis g a2 | % 181
      g8 b a g a c, b a | % 182
      g g' fis e fis d c b | % 183
      a8 g fis e d b' c d | % 184
      e c fis g a2 | % 185
      \shape #'((.5 . .5)(8 . 2)(16 . 2)(24 . .5)) LaissezVibrerTie
      bes1^\lvlv\laissezVibrer | % 186
      R1 | % 187
      R1 | % 188
      R1 | % 189
      R1 | % 190
      R1 | % 191
      R1 | % 192
      R1 | % 193
      R1 | % 194
      R1 | % 195
      R1 | % 196
      R1 | % 197
      R1 | % 198
      R1 | % 199
      R1 | % 200
      R1 | % 201
      R1 | % 202
      R1 | % 203
      R1 | % 204
      R1 | % 205
      R1 | % 206
      R1 | % 207
      R1 | % 208
      R1 | % 209
      R1 | % 210
      R1 | % 211
      R1 | % 212
      R1 | % 213
      R1 | % 214
      R1 | % 215
      R1 | % 216
      R1 | % 217
      R1 | % 218
      R1 | % 219
      R1 | % 220
      R1 | % 221
      R1 | % 222
      R1 | % 223
      R1 | % 224
      R1 | % 225
      R1 | % 226
    }
  >>
}

oneGuitarIII = \relative c' {
  << \transpose c f \oneGlobal
     % Music follows here.
     \transpose c f
     \relative c'
     {
       g,8^\markup { \hspace #-1.5 \smallCaps "Tuning: B E A D F# B" } g d' d f f f d | % 1
       g,8 g d' d f f f d | % 2
       g,8 g d' d f f f d | % 3
       bes bes bes bes c c c c | % 4
       g8 g d' d f f f d | % 5
       g,8 g d' d f f f d | % 6
       g,8 g d' d f f f d | % 7
       bes bes bes bes c c c c | % 8
       g8 g d' d f f f d | % 9
       g,8 g d' d f f f d | % 10
       g,8 g d' d f f f d | % 11
       bes bes bes bes c c c c | % 12
       g8 g d' d f f f d | % 13
       g,8 g d' d f f f d | % 14
       g,8 g d' d f f f d | % 15
       bes bes bes bes c c c c | % 16
       g8 g d' d f f f d | % 17
       g,8 g d' d f f f d | % 18
       g,8 g d' d f f f d | % 19
       bes bes bes bes c c c c | % 20
       g8 g d' d f f f d | % 21
       g,8 g d' d f f f d | % 22
       g,8 g d' d f f f d | % 23
       bes bes bes bes c c c c | % 24
       g8 g d' d f f f d | % 25
       g,8 g d' d f f f d | % 26
       g,8 g d' d f f f d | % 27
       bes bes bes bes c c c c | % 28
       g8 g d' d f f f d | % 29
       g,8 g d' d f f f d | % 30
       g,8 g d' d f f f d | % 31
       bes bes bes bes c c c c | % 32
       g8 g d' d f f f d | % 33
       g,8 g d' d f f f d | % 34
       g,8 g d' d f f f d | % 35
       bes bes bes bes c c c c | % 36
       g8 g d' d f f f d | % 37
       g,8 g d' d f f f d | % 38
       g,8 g d' d f f f d | % 39
       bes bes bes bes c c c c | % 40
       g8 g d' d f f f d | % 41
       g,8 g d' d f f f d | % 42
       g,8 g d' d f f f d | % 43
       bes bes bes bes c c c c | % 44
       g8 g d' d f f f d | % 45
       g,8 g d' d f f f d | % 46
       g,8 g d' d f f f d | % 47
       bes bes bes bes c c c c | % 48
       g8 g d' d f f f d | % 49
       g,8 g d' d f f f d | % 50
       g,8 g d' d f f f d | % 51
       bes bes bes bes c c c c | % 52
       g8 g d' d f f f d | % 53
       g,8 g d' d f f f d | % 54
       g,8 g d' d f f f d | % 55
       bes bes bes bes c c c c | % 56
       g8 g d' d f f f d | % 57
       g,8 g d' d f f f d | % 58
       g,8 g d' d f f f d | % 59
       bes bes bes bes c c c c | % 60
       g8 g d' d f f f d | % 61
       g,8 g d' d f f f d | % 62
       g,8 g d' d f f f d | % 63
       bes bes bes bes c c c c | % 64
       g8 g d' d f f f d | % 65
       g,8 g d' d f f f d | % 66
       g,8 g d' d f f f d | % 67
       bes bes bes bes c c c c | % 68
       g8 g d' d f f f d | % 69
       g,8 g d' d f f f d | % 70
       g,8 g d' d f f f d | % 71
       bes bes bes bes c c c c | % 72
       g8 g d' d f f f d | % 73
       g,8 g d' d f f f d | % 74
       g,8 g d' d f f f d | % 75
       g, g d' d f f f d | % 76
       R1 | % 77
       R1 | % 78
       R1 | % 79
       R1 | % 80
       bes8 bes bes bes g g g g | % 81
       es es es es a a a a | % 82
       bes bes bes bes g g g g | % 83
       es es es es a a a a | % 84
       bes bes bes bes g g g g | % 85
       es es es es a a a a | % 86
       bes bes bes bes g g g g | % 87
       es es es es a a a a | % 88
       \override TextSpanner.bound-details.left.text = \markup \italic "palm mute"
       bes\startTextSpan r g r es r a r | % 89
       bes r g r es r a r\stopTextSpan | % 90
       R1 | % 91
       R1 | % 92
       R1 | % 93
       R1 | % 94
       R1 | % 95
       R1 | % 96
       R1 | % 97
       R1 | % 98
       R1 | % 99
       R1 | % 100
       R1 | % 101
       R1 | % 102
       R1 | % 103
       r2 d, | % 104
       g f | % 105
       bes es, | % 106
       f f' | % 107
       bes, a | % 108
       g f | % 109
       es d | % 110
       c f | % 111
       bes a | % 112
       g2 f | % 113
       es d | % 114
       c fis | % 115
       g f! | % 116
       bes es, | % 117
       f f8 f f f | % 118
       \repeat unfold 3 { \relative c { g g d' d es es es c } } | % 119-121
       %\spanish "heavy distortion"
      
       des'^\markup \smallCaps "heavy distortion" des des bes des des des c, | % 122
       des' des des bes des des des c, | % 123
       R1 | % 124
       des'8 des des bes des des des c, | % 125
       R1 | % 126
       des'8 c, des' c, r2 | % 127
       des'8 c, r4 des'8 c, r4 | % 128
       r4 des'8 c, r4 | % 129
       \repeat unfold 3 { \relative c, { f8 f c' c es es es c } } | % 130-132
       as'8 as as as bes bes bes bes | % 133
       \repeat unfold 3 { \relative c, { f8 f c' c es es es c } } | % 134-136
       as8 as as as bes bes bes bes | % 137
       f f c' c es es es c | % 138
       \repeat unfold 2 { \relative c, { f8 f c' c es es es c } } | % 139-140
       as8 as as as bes bes bes bes | % 141
       \repeat unfold 3 { \relative c, { f8 f c' c es es es c } } | % 142-144
       a a c c f f f c | % 145
       des des des c des des des bes | % 146
       c8 c, c c c c c c | % 147
       f f f f es es es es | % 148
       des des des des des des des des | % 149
       des des des des des des des des | % 150
       es es es es es es es es | % 151
       es es es es es es es es | % 152
       \repeat percent 3 { \relative c, { f f c' c des des des bes } } | % 153-155
       fis fis fis fis gis gis gis gis | % 156
       \repeat unfold 3 { \relative c, { dis dis ais' ais cis cis cis ais } } | % 157-159
       eis eis b' b cis cis cis cis, | % 160
       fis fis cis' cis eis eis eis cis | % 161
       e e e cis! d d d cis | % 162
       b b b b g g g g | % 163
       fis! fis fis fis g g g g | % 164
       e e e e c c c c | % 165
       d d d d d d d d | % 166
       g!2 fis! | % 167
       e2 d | % 168
       c' b | % 169
       a d, | % 170
       g fis | % 171
       e d | % 172
       c' b | % 173
       a b | % 174
       e, d | % 175
       g c | % 176
       d d, | % 177
       g fis | % 178
       e d | % 179
       c' b  | % 180
       a d, | % 181
       g fis | % 182
       e d | % 183
       c'2 b | % 184
       a d, | % 185
       \repeat unfold 4
       {
         \repeat percent 3 { \relative c { g8 g d' d f f f d } } 
         bes'8 bes bes bes c c c c
       } | % 186-201
       R1 | % 202
       R1 | % 203
       R1 | % 204
       R1 | % 205
       R1 | % 206
       R1 | % 207
       R1 | % 208
       R1 | % 209
       R1 | % 210
       R1 | % 211
       R1 | % 212
       R1 | % 213
       R1 | % 214
       R1 | % 215
       R1 | % 216
       R1 | % 217
       R1 | % 218
       R1 | % 219
       R1 | % 220
       R1 | % 221
       R1 | % 222
       R1 | % 223
       R1 | % 224
       R1 | % 225
       R1 | % 226
      
     }
  >>
}



oneMezzoIPart = \new Staff \with {
  instrumentName = "Sarah"
  midiInstrument = "flute"

} << \new voice = "SarahVoice" { \oneMezzoI } 
     \addlyrics { \oneMezzoILyrics }
>>

oneMezzoIIPart = \new Staff \with {
  instrumentName = "Jasmine"
  midiInstrument = "flute"
} { \oneMezzoII }
\addlyrics { \oneMezzoIILyrics }


oneGuitarIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Guitar I"
} { \clef "treble_8" \oneGuitarI  }

oneGuitarIIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Guitar II"
} { \clef "treble_8" \oneGuitarII }

oneGuitarIIIPart = \new Staff \with {
  midiInstrument = "acoustic guitar (nylon)"
  instrumentName = "Baritone Guitar"
} { \clef "treble_8" \transpose c g { \oneGuitarIII } }


allSingers = \new StaffGroup <<
  \new Staff = "Sarah" \with {
    instrumentName = "Sarah"
    midiInstrument = "choir aahs"
  } << 
    \new Voice = "SV" { \oneMezzoI }
    \new Lyrics \lyricsto "SV" { \oneMezzoILyrics }
  >>
  \new Staff = "Jasmine" \with {
    instrumentName = "Jasmine"
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "JV" { \oneMezzoII }
    \new Lyrics \lyricsto "JV" { \oneMezzoIILyrics }
  >>
>>

allGuitars = \new StaffGroup <<
  \new Staff = "GuitarIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar I"
  } { \clef "treble_8" \oneGuitarI }

  \new Staff ="GuitarIIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Guitar II"
  } { \clef "treble_8" \oneGuitarII }

  \new Staff = "GuitarIIIPart" \with {
    midiInstrument = "acoustic guitar (nylon)"
    instrumentName = "Baritone Guitar"
  } { 
    \set Staff.clefGlyph = #"clefs.G"
    \set Staff.clefPosition = #-2
    \set Staff.clefTransposition = #-10
    \set Staff.middleCPosition = #1
    \set Staff.middleCClefPosition = #1
    \oneGuitarIII }

>>

\score {
  <<
    \allSingers
    \allGuitars

  >>
  \layout { }
  \midi {
    \tempo 2.=50
  }
}
